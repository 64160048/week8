package com.thitiphong.week8;

public class Circle {
    private String name;
    private int radius;
    
    public Circle(String name, int radius){
        this.name = name;
        this.radius = radius;
    }
    public String getNameCir() {
        return name ;
    }        
    public int getRadiusCir() {
        return radius ;
    }       
    void CirArea() {
        System.out.println((name) + ("Area = ") + (Math.PI * (radius*radius)));
    }    
    void CirPerimeter() {
        System.out.println((name) + (" Perimeter = ") + (2*Math.PI*radius));
    }
    void CirInfo() {
        System.out.println("Name:" + name + " Radius:" + radius );
    }


}
